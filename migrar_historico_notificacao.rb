
def migrate_history(clients)
  clients.each do |client|
    params = {"client_id" => client.id}
    client.sac.historics.each do |historic|
      historic_fields = historic.attributes.slice("user_id", "status", "call_back_at", "description", "updated_at", "created_at")
      params = {"client_id" => client.id}.merge(historic_fields)

      SacEntry.create!(params)
    end
  end
end

def create_notifications(users)
  users = users.to_a
  
  users.each do |user|
    puts "User(#{users.index(user)+1}/#{users.size}): #{user.name}(#{user.login})"

    clients = SacEntry.where(user: user).map(&:client).uniq
    puts "\tClients: #{clients.count}"

    clients.each do |client|
      last_entry = SacEntry.where(user_id: user.id, client: client).first
      if last_entry && last_entry.call_back_at
        NotificationScheduleManager.new(last_entry.client, user.id, last_entry.call_back_at)
          .process 
      end
    end

    puts "\tNotifications: #{Notification.where(user: user).count}"
    puts "\tDone!\n\n"
  end
end

SacEntry.destroy_all
migrate_history(Client.all)

Notification.destroy_all
create_notifications(User.where(locked_at: nil))



















# ================================== EXEMPLO ==================================
