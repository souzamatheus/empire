puts 'SETTING UP DEFAULT ADMIN LOGIN'
user = User.new
  user.login = 'admin'
  user.name = 'Administrador'
  user.password = 'adminempire'
  user.password_confirmation = 'adminempire'
  user.role = 'admin'
  user.save
puts 'New user created: ' << user.login

class Sequence
  include Mongoid::Document
  store_in collection: "__sequences"
end

sequence = Sequence.find("service_document_number")
if sequence.seq < 5000
  sequence.set(:seq, 4999)
  puts "Set sequence for service_document_number"
end