class InpiWorkerManager

  def initialize(inpi_file_url)
    @inpi_file_url = inpi_file_url
  end

  def valid?
    valid_file_url? && is_a_new_file?
  end

  private

  def valid_file_url?
    !@inpi_file_url.match(/^http:\/\/revistas\.inpi\.gov\.br\/txt\/(RM|rm)(\d+)\.zip$/).nil?  
  end

  def is_a_new_file?
    Inpi::File.where(name: filename).empty?
  end

  def filename
    file_number = File.basename(@inpi_file_url).match(/(\d+)/)
    "#{file_number}.xml"
  end
end
