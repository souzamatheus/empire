class OutdatedClientProcessCacheManager
  @@notifications = nil

  def self.notifications
    return @@notifications[:data] if @@notifications && @@notifications[:date] == Date.current
    @@notifications = {date: Date.current, data: build_notifications}
    @@notifications[:data]
  end

  def self.reset!
    @@notifications = nil
  end

  private

  def self.build_notifications
    extraordinary_clients = Client.where('client_processes.start_extraordinary_appeal' => {:$lte => Time.current.beginning_of_day})
    ordinary_clients = Client.where('client_processes.start_ordinary_appeal' => {:$lte => Time.current.beginning_of_day})
    ordinary_clients = ordinary_clients - extraordinary_clients

    time = Date.current.to_time.change(hour: 6, min: 0, sec: 0)
    notifications = extraordinary_clients.map do |client|
      Notification.new(
        client: client, status: Notification::Status::DELAYED,
        call_back_at: time, notification_type: Notification::Type::PROCESS_VALIDITY
      )
    end

    notifications += ordinary_clients.map do |client|
      Notification.new(
        client: client, status: Notification::Status::INTIME,
        call_back_at: time, notification_type: Notification::Type::PROCESS_VALIDITY
      )
    end
    notifications
  end

end
