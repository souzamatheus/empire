class ServiceDocumentManager
  attr_accessor :client, :document

  def initialize(client, params)
    @client = client
    @document = ServiceDocument.new(params)
    fill_with_client_informations
  end

  def save
    @document.save
  end

  private
  #TODO refatorar essa parte do tipo do documento do cliente
  # tanto aqui quanto no model do cliente
  def fill_with_client_informations
    @document.client_id       = @client.id
    @document.company_name    = @client.company_name
    @document.address         = @client.address
    @document.address_number  = @client.number
    @document.complement      = @client.complement
    @document.neighborhood    = @client.neighborhood
    @document.city            = @client.city
    @document.state           = @client.state
    @document.postal_code     = @client.postal_code
    @document.client_document = if @client.document_type == "company"
      @client.cnpj
    else
      @client.cpf
    end
    @document.phone   = @client.primary_phone
    @document.contact = @client.contact
  end
end
