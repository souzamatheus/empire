class NotificationProcessManager
  def initialize(client, status=nil)
    @client = client
    @status = status
  end

  def process
    return false if @status.nil?

    clean
    create
  end

  def clean
    notification = Notification.where(
	    		client: @client,
		       	notification_type: Notification::Type::PROCESS_VALIDITY).first

    return false if notification.nil?
    notification.destroy
  end

  private

  def create
    Notification.create(client: @client,
			status: @status,
			call_back_at: Time.now,
			notification_type: Notification::Type::PROCESS_VALIDITY)
  end
end
