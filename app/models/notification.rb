# encoding: UTF-8

class Notification
  include Mongoid::Document
  include Mongoid::Timestamps
  include EnumerateIt
  
    class Type < EnumerateIt::Base
      associate_values(
        :scheduling       => [1,  'Agendamento'],
	:process_validity => [2,  'Vigência de processo']
      )
    end

    class Status < EnumerateIt::Base
      associate_values(
        :delayed, :intime
      )
    end

  field :call_back_at, type: DateTime
  field :notification_type, type: Integer, default: Notification::Type::SCHEDULING
  field :status

  has_enumeration_for :notification_type, with: Notification::Type, required: true, create_scopes: true
  has_enumeration_for :status, with: Notification::Status

  validates :call_back_at, presence: true

  belongs_to :user
  belongs_to :client

  def self.for(user)
    where(user_id: user.id,
          call_back_at: {
            :$lte => 10.minutes.from_now
          })
  end
end
