#encoding: utf-8
class Client
  include Mongoid::Document
  include MediaMagick::Model
  include Mongoid::Slug
  include Mongoid::Timestamps
  include Mongoid::Paranoia

  ANNUITIES = [ ["Maio", "may"], ["Novembro", "november"] ]
  TRANSLATE_ANNUITY = {
    "may" => "Maio",
    "november" => "Novembro"
  }
  DOCUMENT_TYPE = {"Físico" => :people, "Jurídico" => :company}

  field :company_name
  field :brand
  field :document_type, default: :company
  field :cnpj
  field :cpf
  field :annuity
  field :email
  field :site

  field :address
  field :number
  field :complement
  field :neighborhood
  field :city
  field :state
  field :postal_code

  field :primary_phone
  field :secondary_phone
  field :contact

  field :status, default: 'active'
  field :status_refused, default: 'unanswered'

  slug :company_name

  belongs_to :user
  has_many :client_files, dependent: :destroy
  has_many :service_documents, dependent: :destroy
  has_many :client_images, dependent: :destroy
  has_many :sac_entries, dependent: :destroy

  embeds_one :sac, autobuild: true
  embeds_many :client_processes do
    def active
      where(:number.ne => nil)
    end
  end
  embeds_many :client_emails
  embeds_many :brands
  embeds_many :conflicts

  accepts_nested_attributes_for :client_files, allow_destroy: true
  accepts_nested_attributes_for :client_images, allow_destroy: true
  accepts_nested_attributes_for :client_processes,  reject_if: proc { |attributes| attributes["number"].blank? }, allow_destroy: true
  accepts_nested_attributes_for :client_emails,     reject_if: proc { |attributes| attributes["email"].blank?  }, allow_destroy: true
  accepts_nested_attributes_for :brands,            reject_if: proc { |attributes| attributes["name"].blank?  }, allow_destroy: true
  accepts_nested_attributes_for :conflicts,         reject_if: proc { |attributes| attributes["value"].blank?  }, allow_destroy: true

  validates :company_name, :primary_phone, :user_id, :contact, presence: true
  validates :annuity, presence: true, if: :is_an_active_client?
  validates :annuity, inclusion: { in: TRANSLATE_ANNUITY.keys }, if: :is_an_active_client?

  validates_with ClientValidator

  scope :latest, order_by([['sac.call_back_at', 'desc'], ['company_name', 'asc']])

  before_save :clean_document
  after_initialize :set_up_client
  after_destroy :reset_outedated_client_process_cache

  def translate_annuity
    TRANSLATE_ANNUITY[self.annuity]
  end

  def self.find_conflict(query)
    scoped.in(status: [
              'active',
              'entry_active',
              'entry_cancelled',
              'without_monitoring']).
    or(
      {"conflicts.value" => /^#{Accent.to_regexp(query)}$/i},
      {"brands.name" => /^#{Accent.to_regexp(query)}$/i}
    )
  end

  def self.filter options={}
    options.inject(scoped) do |relation, hash|
      key, value = hash
      relation = send("by_#{key}", value, relation) if value.present?
      relation
    end
  end

  def self.by_annuity month, relation
    return relation unless month.present?

    relation.where(annuity: month)
  end

  def self.by_status status, relation
    return relation unless status.present?

    relation.or({status: status},
                {status_refused: status})
  end

  def self.by_document_type document, relation
    return relation unless document.present?

    relation.where(document_type: document)
  end

  def self.by_user_id user_id, relation
    return relation unless user_id.present?

    relation.where(user_id: user_id)
  end

  def self.by_search query, relation
    return relation unless query.present?

    relation.or({company_name: /#{Accent.to_regexp(query)}/i},
                {cpf:   /#{Client.clean_string(query)}/i},
                {cnpj:  /#{Client.clean_string(query)}/i},
                {brand: /#{Accent.to_regexp(query)}/i}, # Remover após a implantação
                {"brands.name" => /#{Accent.to_regexp(query)}/i})
  end

  def self.by_atendiment_status status, relation
    return relation unless status.present?

    relation.where('sac.status' => status.to_i)
  end

  def self.by_atendiment_date dates, relation
    start_date, finish_date = dates
    started_at, finished_at = formated_date(start_date), formated_date(finish_date)

    return relation unless started_at.present?

    relation.where('sac.historics.created_at' => (started_at.utc.beginning_of_day..(finished_at || Time.now).utc.end_of_day))
  end

  def self.formated_date date
    if date.present?
      day, month, year = date.split('/')
      Time.new(year.to_i, month.to_i, day.to_i)
    end
  end

  def validate_company_doc?
    document_type.to_sym == :company
  end

  def validate_people_doc?
    document_type.to_sym == :people
  end

  private

  def is_an_active_client?
    active? || entry_active? || without_monitoring?
  end

  def set_up_client
    client_emails.build     if client_emails.empty?
    client_processes.build  if client_processes.empty?
    brands.build            if brands.empty?
    conflicts.build         if conflicts.empty?
  end

  def clean_document
    clean_cpf  if document_type.to_sym == :people
    clean_cnpj if document_type.to_sym == :company
  end

  def clean_cpf
    self.cpf = Client.clean_string(self.cpf)
  end

  def clean_cnpj
    self.cnpj = Client.clean_string(self.cnpj)
  end

  def self.clean_string(string)
    string.gsub('.', '').gsub('-', '').gsub('/', '')
  end

  def reset_outedated_client_process_cache
    OutdatedClientProcessCacheManager.reset!
  end

  state_machine :status, :initial => :negotiating   do
    after_transition :refused => :active, do: :unanswer

    event :cancel do
      transition [:active, :entry_active, :without_monitoring] => :canceled
    end

    event :refuse do
      transition [:active, :negotiating] => :refused
    end

    event :negotiate do
      transition :active => :negotiating
    end

    event :success do
      transition [:without_monitoring, :entry_active, :negotiating, :canceled, :refused] => :active
    end

    event :active_entry do
      transition [:without_monitoring, :active, :entry_cancelled, :negotiating, :canceled, :refused] => :entry_active
    end

    event :cancel_entry do
      transition [:without_monitoring, :active, :entry_active, :negotiating, :canceled, :refused] => :entry_cancelled
    end

    event :unfollow do
      transition [:active, :entry_active, :entry_cancelled, :negotiating, :canceled, :refused] => :without_monitoring
    end

    state :active
    state :negotiating
    state :canceled
    state :refused
    state :entry_active
    state :entry_cancelled
    state :without_monitoring
  end

  state_machine :status_refused, :initial => :unanswered do
    event :refused_by_service_price do
      transition :unanswered => :expensive_price
    end

    event :refused_by_checking_price do
      transition :unanswered => :checked_price
    end

    event :refused_by_postpone do
      transition :unanswered => :postponed
    end

    event :refused_by_without_autonomy do
      transition :unanswered => :not_autonomy
    end

    event :unanswer do
      transition [:expensive_price,
                  :checked_price  ,
                  :postponed      ,
                  :not_autonomy   ] => :unanswered
    end

    state :expensive_price
    state :checked_price
    state :postponed
    state :not_autonomy
    state :unanswered
  end
end
