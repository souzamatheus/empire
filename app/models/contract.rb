class Contract
  include Mongoid::Document

  field :payment

  validates :payment, presence: true

  embedded_in :service_document
end
