class Historic
  include Mongoid::Document
  include Mongoid::Timestamps
  include EnumerateIt

  field :description
  field :status
  field :call_back_at, type: DateTime

  validates :description, :status, presence: true

  has_enumeration_for :status, :with => SacStatus
  embedded_in :sac
  belongs_to :user
end