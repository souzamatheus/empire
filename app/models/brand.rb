class Brand
  include Mongoid::Document

  field :name
  embedded_in :client
end
