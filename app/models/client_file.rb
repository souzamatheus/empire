class ClientFile
  include Mongoid::Document
  include Mongoid::Timestamps

  field :filename

  mount_uploader :file, ClientUploader
  belongs_to :client
end