module Inpi
  class Entry
    include Mongoid::Document

    field :number
    field :date
    field :class_name
    field :company_name
    field :inpi
    field :presentation
    field :nature
    field :brand
    field :conflict

    validates :number, :uniqueness => {:scope => :file}

    belongs_to :client
    belongs_to :file, class_name: 'Inpi::File'
  end
end
