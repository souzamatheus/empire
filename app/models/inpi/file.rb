module Inpi
  class File
    include Mongoid::Document
    include Mongoid::Slug
    include Mongoid::Timestamps

    field :name
    field :status
    field :finished_at, type: DateTime

    slug :name

    validates :name, presence: true
    validates :name, uniqueness: true

    has_many :entries, class_name: "Inpi::Entry", dependent: :destroy

    state_machine :status, :initial => :waiting do
      state :finished

      after_transition :to => :finished do |transition|
        transition.update_attribute(:finished_at, Time.now)
      end

      event :finish do
        transition :waiting => :finished
      end

      event :rollback do
        transition :finished => :waiting
      end
    end
  end
end
