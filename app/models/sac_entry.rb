class SacEntry
  include Mongoid::Document
  include Mongoid::Timestamps
  include EnumerateIt

  field :description
  field :status
  field :call_back_at, type: DateTime

  validates :description, :user_id, :client_id, presence: true
  validates_length_of :description, minimum: 10

  has_enumeration_for :status, with: SacStatus, required: true

  belongs_to :user
  belongs_to :client

  default_scope order_by(:created_at.desc)
end
