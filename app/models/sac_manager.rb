class SacManager

  def initialize(client, entry)
    @client, @entry = client, entry
    @entry[:status] = @entry[:status].to_i
  end

  def save
    entry = @client.sac_entries.new(@entry)

    if entry.save
      NotificationScheduleManager
        .new(@client, @entry[:user_id], @entry[:call_back_at])
        .process
    end
  end
end
