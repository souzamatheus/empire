class ClientProcess
  include Mongoid::Document

  attr_accessible :number, :filing_at, :grant_at, :validity_at

  field :number
  field :filing_at, type: Date
  field :grant_at, type: Date
  field :validity_at, type: Date
  field :start_ordinary_appeal, type: Date
  field :finish_ordinary_appeal, type: Date
  field :start_extraordinary_appeal, type: Date
  field :finish_extraordinary_appeal, type: Date

  embedded_in :client

  before_save :fill_appeal_dates, if: ->{self.validity_at_changed?}
  after_save :reset_outedated_client_process_cache
  after_destroy :reset_outedated_client_process_cache

  private

  def reset_outedated_client_process_cache
    OutdatedClientProcessCacheManager.reset!
  end

  def fill_appeal_dates
    self.start_ordinary_appeal = self.validity_at - 1.year + 1.day
    self.finish_ordinary_appeal = self.validity_at
    self.start_extraordinary_appeal = self.validity_at + 1.day
    self.finish_extraordinary_appeal = self.validity_at + 6.months
  end
end
