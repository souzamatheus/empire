class User
  include Mongoid::Document
  include Mongoid::Slug

  ROLES = [ ["Administrador", "admin"], ["Supervisor", "manager"], ["Consultor", "consultant"], ["Atendente", "operator"] ]

  TRANSLATE_ROLE = {
    "admin" => "Administrador",
    "manager" => "Supervisor",
    "consultant" => "Consultor",
    "operator" => "Atendente"
  }

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :trackable, :lockable

  ## Database authenticatable
  field :login,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""
  field :role
  field :name
  field :locked_at, :type => DateTime

  validates_presence_of     :password, :if => :password_required?
  validates_confirmation_of :password#, :if => :password_required?
  validates_length_of       :password, :within => Devise.password_length, :allow_blank => true

  validates_presence_of :encrypted_password

  validates :role, inclusion: { in: TRANSLATE_ROLE.keys }
  validates :login, :name, :role, presence: true
  validates :login, uniqueness: true

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  # field :confirmation_token,   :type => String
  # field :confirmed_at,         :type => Time
  # field :confirmation_sent_at, :type => Time
  # field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String

  field :constraints, type: Hash, default: { blocked: false, client: nil }
  has_many :clients

  before_validation :set_password, if: :new_record?

  slug :login

  def translate_role
    TRANSLATE_ROLE[self.role]
  end

  def lock(client)
    self.update_attribute :constraints, { blocked: true, client: client.id }
  end

  def unlock
    self.update_attribute :constraints, { blocked: false, client: nil }
  end

  def reset_password
    initial_password
    save
  end

  def lockable?
    %w(consultant operator).include?(role)
  end

  # Configuração do Devise que verifica a cada request
  # se o usuário ainda está ativo. Colocando a verificação de horário de
  # funcionamento aqui não precisamos de before_filter no ApplicationController
  # ref.:https://github.com/plataformatec/devise/wiki/How-To:-Customize-user-account-status-validation-when-logging-in
  def active_for_authentication?
    super && !access_locked? && (self.role == 'admin' || Settings.open?)
  end

  protected

  def password_required?
    !persisted? && (!password.nil? && !password_confirmation.nil?)
  end

  private

  def initial_password
    self.password = "#{self.login}empire"
  end

  def set_password
    initial_password if password.blank?
  end
end
