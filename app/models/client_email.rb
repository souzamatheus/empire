class ClientEmail
  include Mongoid::Document

  field :email
  embedded_in :client

  validates_uniqueness_of :email, :allow_blank => true, :if => :email_changed?
  validates_format_of     :email, :with  => Devise.email_regexp, :allow_blank => true, :if => :email_changed?
 end
