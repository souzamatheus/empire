class Sac
  include Mongoid::Document
  include Mongoid::Timestamps
  include EnumerateIt

  field :status
  field :call_back_at, type: DateTime

  belongs_to :user

  has_enumeration_for :status, :with => SacStatus
  embeds_many :historics, order: [:created_at, -1]
  embedded_in :client
end