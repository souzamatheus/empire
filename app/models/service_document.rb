# encoding: UTF-8
class ServiceDocument
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Sequence
  include Mongoid::Paranoia
  include EnumerateIt

  # DADOS DE SERVIÇO
  field :number, type: Integer
  field :description
  field :agency
  field :amount, type: BigDecimal
  field :payment

  # OUTORGANTE
  field :person_name
  field :nationality
  field :relationship_status
  field :rg
  field :cpf
  field :birth_date, type: Date
  field :occupation
  field :position
  field :observations
  field :agent

  # INFOS DO CLIENTE
  field :company_name
  field :address
  field :address_number
  field :complement
  field :neighborhood
  field :city
  field :state
  field :postal_code
  field :client_document
  field :phone
  field :contact

  sequence :number

  validates :number, :description, :agency, :amount, :payment,
          :person_name, :nationality, :relationship_status, :rg,
          :cpf, :birth_date, :occupation, :position, :agent, :company_name,
          :address, :neighborhood, :city, :state, :postal_code,
          :client_document, :phone, :contact, presence: true

  validate :validate_cpf

  embeds_one :contract
  belongs_to :client
  after_initialize :convert_relationship_status

  class RelationshipStatus < EnumerateIt::Base
    associate_values(
      :single   => [1, 'Solteiro(a)'],
      :married  => [2, 'Casado(a)'],
      :widow    => [3, 'Viúvo(a)'],
      :divorced => [4, 'Divorciado(a)']
    )
  end

  has_enumeration_for :relationship_status, with: RelationshipStatus

  def full_address
    result = "#{address}"
    result << ", #{address_number}" if address_number.present?
    result << ", #{complement}"     if complement.present?
    result
  end

  private

  def validate_cpf
    require 'cpf_cnpj'
    errors.add(:cpf, :invalid) if !CPF.valid?(cpf)
  end

  def convert_relationship_status
    self.relationship_status = self.relationship_status.to_i
  end
end
