# encoding: UTF-8
class DocumentsController < ApplicationController
  authorize_resource :service_document
  before_filter :load_client

  def index
    @documents = @client.service_documents
  end

  def show
    @document = @client.service_documents.find(params[:id])
    send_data Generator::Authorization::Base.new(@document).generate,
              type:        "application/pdf",
              filename:    "Autorização.pdf",
              disposition: "inline"
  end

  def new
    last = ServiceDocument.where(client_id: @client.id).last
    if last
      @document = ServiceDocument.new(
        person_name: last.person_name,
        nationality: last.nationality,
        relationship_status: last.relationship_status,
        rg: last.rg,
        cpf: last.cpf,
        birth_date: last.birth_date,
        occupation: last.occupation,
        position: last.position
      )
    else
      @document = ServiceDocument.new
    end  
  end

  def create
    manager   = ServiceDocumentManager.new(@client, params[:service_document])
    @document = manager.document

    if manager.save
      TrackerDocument.register(@client, current_user, "ServiceDocument", "create")
      redirect_to service_authorizations_path(@client, anchor: :auth), :notice => "Autorização de serviço criada com sucesso."
    else
      render "new"
    end
  end

  def edit
    
  end

  def destroy
    @document = @client.service_documents.find(params[:id])
    @document.destroy

    redirect_to service_authorizations_path(@client), :notice => "Autorização de serviço removida com sucesso."
  end

  private

  def load_client
    @client = Client.find(params[:client_id])

    @sac = Sac.new
    @sac.historics.build
  end
end
