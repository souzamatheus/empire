#encoding: utf-8
class ClientsProcessesController < ApplicationController
  before_filter :load_client
  before_filter :load_client_process, only: [:edit, :update, :destroy]

  def index
  end

  def new
    @process = @client.client_processes.build
  end

  def create
    if @client.client_processes.create(params[:client_process])
      flash[:notice] = 'Processo criado com sucesso.'
    else
      flash[:error]  = 'Houve um erro. Verifique todos os campos.'
    end

    redirect_to sac_path(@client, anchor: :process)
  end

  def edit
  end

  def update
    if @process.update_attributes(params[:client_process])
      NotificationProcessManager.new(@client).clean
      flash[:notice] = 'Processo atualizado com sucesso.'
    else
      flash[:error]  = 'Houve um erro. Verifique todos os campos.'
    end

    redirect_to sac_path(@client, anchor: :process)
  end

  def destroy
    @process.destroy

    redirect_to sac_path(@client, anchor: :process), notice: 'Processo removido com sucesso.'
  end

  private

  def load_client
    @client = Client.find(params[:client_id])
    @sac = Sac.new
    @sac.historics.build
  end

  def load_client_process
    @process = @client.client_processes.find(params[:id])
  end
end
