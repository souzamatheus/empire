# encoding:utf-8
class AwaresController < ApplicationController
  before_filter :load_client

  def show
    send_data Generator::AwareDocument::Base.new(@client).generate,
              type:        "application/pdf",
              filename:    "carta_de_ciencia.pdf",
              disposition: "inline"
  end

  private

  def load_client
    @client = Client.find(params[:client_id])
    @sac = Sac.new
    @sac.historics.build
  end
end
