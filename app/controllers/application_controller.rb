# encoding: UTF-8
class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => "Você não está autorizado a acessar essa página."
  end

  before_filter :authenticate_user!, :check_if_user_is_locked

  protect_from_forgery

  private

  def after_sign_in_path_for(resource)
    clients_path
  end

  def check_if_user_is_locked
    if user_signed_in?
      go_back_to_client if current_user.constraints["blocked"]
    end
  end

  def go_back_to_client
    if restrict_path?
      redirect_to sac_path(current_user.constraints["client"]), notice: blocked_message
    end
  end

  def blocked_message
    "Você precisa deixar um histórico de atendimento nesse cliente!"
  end

  def restrict_path?
    client = Client.find(current_user.constraints["client"].to_s)
    services_path = "servicos"

    (
      request.fullpath.match(client.slug).nil? &&
      request.fullpath.match(client.id.to_s).nil?
    ) && request.fullpath.match(services_path).nil?
  end
end
