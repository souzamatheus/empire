class SacController < ApplicationController

  before_filter :load_client
  before_filter :lock_user, only: :show

  def show
  end

  def create
    @manager = SacManager.new @client, params[:sac_entry]
    @manager.save ? unlock_and_redirect : render_error
  end

  private

  def load_client
    @client    = Client.find(params[:id])
    @documents = @client.service_documents
  end

  def lock_user
    current_user.lock(@client) if referer_is_not_show_path and current_user.lockable?
  end

  # Verifica se a URL é diferente do atendimento do cliente,
  # baseado no slug ou ID (o ID aparece na URL quando o operador já está
  # bloqueado)
  def referer_is_not_show_path
    request.referer != sac_url(@client) && request.referer != sac_url(@client.id)
  end

  def unlock_and_redirect
    current_user.unlock
    redirect_to sac_path(@client), notice: "Atendimento registrado com sucesso!"
  end

  def render_error
    flash[:error] = "Houve algum problema. Verifique os campos."
    render :show
  end
end
