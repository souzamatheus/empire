class ClientsController < ApplicationController
  before_filter :load_client, :except => [:index, :new, :create, :filter]

  def index
    @clients = Client.filter(user_id: user_to_filter).latest.paginate(:page => params[:page])
  end

  def filter
    @clients = Client.filter(
                          search:  params[:search],
                          user_id: user_to_filter
                        ).paginate(:page => params[:page], :per_page => 30 )
    render 'index'

  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.new params[:client]
    if @client.save
      redirect_to sac_path(@client), :notice => "Cliente criado com sucesso!"
    else
      render "new"
    end
  end

  def edit
    @sac = Sac.new
    @sac.historics.build

    @client.client_images.build
  end

  def update
    if params[:client].has_key?(:client_images_attributes)
      client_attributes = ClientFileManager.prepare_attributes(params[:client],
                                                              :client_images_attributes)
    else
      client_attributes = ClientFileManager.prepare_attributes(params[:client])
    end

    if @client.update_attributes(client_attributes)
      current_user.lock(@client) if current_user.role == "operator"

      if params[:client].has_key?(:client_files_attributes)
        redirect_to sac_path(@client, anchor: :documents), :notice => "Novo documento adicionado com sucesso!"
      else
        redirect_to sac_path(@client), :notice => "Cliente editado com sucesso!"
      end
    else
      render "edit"
    end
  end

  def destroy_file
    @client.client_files.find(params[:file_id]).destroy

    redirect_to sac_path(@client, anchor: :documents), :notice => 'Arquivo Removido com Sucesso'
  end

  def destroy
    @client.destroy
    redirect_to clients_path, :notice => "Cliente removido com sucesso!"
  end

  private

  def load_client
    @client = Client.find(params[:id])

    @sac = Sac.new
    @sac.historics.build
  end

  def user_to_filter
    current_user.id if current_user.role == 'operator'
  end
end
