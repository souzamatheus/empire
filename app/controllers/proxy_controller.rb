class ProxyController < ApplicationController
    before_filter :load_client, :load_document

  def show
    send_data Generator::Proxy::Base.new(@document).generate,
              type:        "application/pdf",
              filename:    "procuracao.pdf",
              disposition: "inline"
  end

  private

  def load_client
    @client = Client.find(params[:client_id])
    @sac = Sac.new
    @sac.historics.build
  end

  def load_document
    @document = @client.service_documents.find(params[:id])
  end
end


