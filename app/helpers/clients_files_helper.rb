module ClientsFilesHelper

  def file_name(client_file)
    if client_file.filename.present?
      client_file.filename
    else
      File.basename("#{client_file.file.path}")
    end
  end
end
