require 'spec_helper'

describe InpiWorkerManager do
  describe '#valid?' do
    context 'with a valid file url' do
      let(:file_url) {'http://revistas.inpi.gov.br/txt/rm2223.zip'}
      before do
        Inpi::File.should_receive(:where)
          .with(name: '2223.xml')
          .and_return(inpi_file)
      end

      context 'with a new file' do
        let(:inpi_file) {[]}
        it {described_class.new(file_url).should be_valid}
      end

      context 'with a exist file' do
        let(:inpi_file) {[double('Inpi::File')]}
        it {described_class.new(file_url).should_not be_valid}
      end
    end
    
    context 'without a valid file url' do
      it {described_class.new('wrong').should_not be_valid}
      it {described_class.new('http://revistas.inpi.gov.br/txt/P2223.zip').should_not be_valid}
      it {described_class.new('http://revistas.inpi.gov.br/txt/rm2223.xml').should_not be_valid}
    end
  end
end
