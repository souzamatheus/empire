require 'spec_helper'

describe NotificationScheduleManager do
  let(:user) { create(:user) }
  let(:client) { create(:client) }

  describe '.process' do
    context 'with call_back_at' do
      let(:manager) { described_class.new(client, user.id, call_back_at) }
      let(:call_back_at) { 20.minutes.from_now }

      context 'with an old notification' do
        context 'with same user' do
          let(:notification) { Notification.last }

          before do
            create(:notification,
                   user: user,
                   client: client,
                   call_back_at: 1.month.ago)
          end

          it { expect{manager.process}.to_not change{Notification.count} }

          it 'must replace the notification' do
            manager.process

            notification.call_back_at.should be_the_same_time_as(call_back_at)
            notification.user.should eq(user)
            notification.client.should eq(client)
          end
        end

        context 'with another user' do
          before do
            create(:notification,
                   user: create(:user),
                   client: client,
                   call_back_at: 1.month.ago)
          end

          it 'must create a new notification' do
            expect{manager.process}.to change{Notification.count}.from(1).to(2)
          end
        end
      end

      context 'without an old notification' do
        it 'must create a new notification' do
          expect{manager.process}.to change{Notification.count}.from(0).to(1)
        end
      end
    end

    context 'without call_back_at' do
      let(:manager) { described_class.new(client, user.id) }

      context 'with an old notification' do
        before do
          create(:notification,
                 user: user,
                 client: client,
                 call_back_at: 1.month.ago)
        end

        it 'must remove the notification' do
          expect{manager.process}.to change{Notification.count}.from(1).to(0)
        end
      end

      context 'without an old notification' do
        it 'must refuse to create a new notification' do
          expect{manager.process}.to_not change{Notification.count}
        end
      end
    end
  end
end
