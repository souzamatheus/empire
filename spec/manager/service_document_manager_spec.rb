require 'spec_helper'

describe ServiceDocumentManager do
  let(:client){ create(:client) }
  let(:document_params){ attributes_for(:service_document) }
  let(:manager){ described_class.new(client, document_params) }

  describe "#initialize" do
    it { manager.client.should be_an_instance_of(Client) }
    it { manager.document.should be_an_instance_of(ServiceDocument) }
    it { manager.document.should be_valid }
  end

  describe "client informations" do
    let(:client){ create :client, address: "Rua tal", number: "134",  complement: "apto 103 bloco 01" }
    subject { manager.document }

    it { subject.client_id.should       == client.id }
    it { subject.company_name.should    == client.company_name }
    it { subject.address.should         == client.address }
    it { subject.address_number.should  == client.number }
    it { subject.complement.should      == client.complement }
    it { subject.neighborhood.should    == client.neighborhood }
    it { subject.city.should            == client.city }
    it { subject.state.should           == client.state }
    it { subject.postal_code.should     == client.postal_code }
    it { subject.client_document.should == client.cpf }
    it { subject.phone.should           == client.primary_phone }
    it { subject.contact.should         == client.contact }
  end

  describe "#save" do
    context 'valid' do
      it { manager.save.should be true }
    end

    context 'invalid' do
      let(:document_params){ {} }
      it { manager.save.should be false }
    end
  end
end
