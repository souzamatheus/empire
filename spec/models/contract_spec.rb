require 'spec_helper'

describe Contract do
  it { should validate_presence_of(:payment) }
  it { should be_embedded_in(:service_document) }
end
