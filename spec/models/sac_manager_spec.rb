require 'spec_helper'

describe SacManager do
  let(:client){ create :client }

  describe '#save' do
    let(:historic){ attributes_for(:sac_entry, user_id: User.last.id) }
    let(:manager){ SacManager.new(client, historic) }

    before do
      notification = double('NotificationSchedulingManager')
      NotificationScheduleManager.should_receive(:new) { notification }
      notification.should_receive(:process)

      manager.save
    end

    context 'new entry' do
      it { client.sac_entries.should_not be_empty }

      context 'fill all the fields' do
        subject { client.sac_entries.last }

        it { subject.user.should == User.last }
        its(:description) { should == historic[:description] }
        its(:status) { should == historic[:status] }
        its(:created_at) { should_not be_nil }
        its(:updated_at) { should_not be_nil }
      end
    end
  end
end
