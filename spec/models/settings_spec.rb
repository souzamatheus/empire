require 'spec_helper'

describe Settings do
  describe 'open?' do
    after {Timecop.return}

    context 'weekday' do
      let(:days) {[16, 17, 18, 19, 20]} # seg-sex

      context 'before 08h45 AM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 8, 44))}
        it {Settings.should_not be_open}
      end

      context '08h45 AM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 8, 45))}
        it {Settings.should be_open}
      end

      context '12h00 PM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 12, 00))}
        it {Settings.should be_open}
      end

      context '18h30 PM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 18, 30))}
        it {Settings.should be_open}
      end

      context 'after 18h30 PM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 18, 31))}
        it {Settings.should_not be_open}
      end
    end

    context 'weekend' do
      let(:days) {[14, 15, 21, 22]} # sab-dom

      context 'before 08h45 AM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 8, 44))}
        it {Settings.should_not be_open}
      end

      context '08h45 AM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 8, 45))}
        it {Settings.should_not be_open}
      end

      context '12h00 PM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 12, 00))}
        it {Settings.should_not be_open}
      end

      context '18h30 PM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 18, 30))}
        it {Settings.should_not be_open}
      end

      context 'after 18h30 PM' do
        before {Timecop.freeze(Time.local(2014, 6, days.sample, 18, 30))}
        it {Settings.should_not be_open}
      end
    end
  end
end
