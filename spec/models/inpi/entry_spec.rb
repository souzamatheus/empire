require 'spec_helper'

describe Inpi::Entry do
  it { should validate_uniqueness_of(:number).scoped_to(:file) }

  it { should belong_to(:client) }
  it { should belong_to(:file) }
end
