require 'spec_helper'

describe Inpi::File do
  it {validate_presence_of(:name)}
  it {validate_uniqueness_of(:name)}

  describe 'state_machine' do
    subject {create(:inpi_file)}

    context 'waiting' do
      it {should be_waiting}
      its(:finished_at) {should be_nil}
    end

    context 'finished' do
      before {subject.finish}

      it {should be_finished}
      its(:finished_at) {should_not be_nil}
    end
  end
end
