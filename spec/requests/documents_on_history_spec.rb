# encoding: UTF-8
require 'spec_helper'

describe "Creating a new entry on history when open some documents" do
  let!(:client){ create(:client, user: user) }

  context 'logged as Consultant or Admin', js: true do
    let(:role) { %w(admin consultant).sample }
    let(:user) { create(:user, role: role) }

    before do
      disable_business_hours
      create(:service_document, :client_informations, client: client)
    end

    # TODO
    # Remover esse teste
    context 'aware document' do
      it "save a information on client's history" do
        user_sign_in
        visit sac_path(client.slug)

        click_link 'Autorização'
        click_link 'Carta de ciência'

        visit sac_path(client.slug)
        within('.table:last-child') do
          expect(page).to_not have_content("Documento visualizado: Carta de Ciência")
          expect(page).to_not have_content(user.name)
          expect(page).to_not have_content("Movimentação interna")
        end
      end
    end

    context 'contract' do
      context 'creating a new contract' do
        it "save a information on client's history" do
          user_sign_in
          visit sac_path(client.slug)

          click_link 'Autorização'
          click_link 'Gerar Contrato'
	  within('#auth') do
            fill_in 'Forma de pagamento', with: '1x R$ 2.000,00'
            click_button('Salvar')
	  end

          expect(page).to have_content("Contrato gerado com sucesso.")

          visit sac_path(client.slug)
          within('.table:last-child') do
            expect(page).to have_content("Documento gerado: Contrato")
            expect(page).to have_content(user.name)
            expect(page).to have_content("Movimentação interna")
          end
        end
      end

      # TODO
      # Remover esse teste
      context 'viewing a contract' do
        before {create(:service_document, :client_informations, client: client, contract: build(:contract))}

        it "save a information on client's history" do
          user_sign_in
          visit sac_path(client.slug)

          click_link 'Autorização'
          click_link 'Ver Contrato'

          visit sac_path(client.slug)
          within('.table:last-child') do
            expect(page).to_not have_content("Documento visualizado: Contrato")
            expect(page).to_not have_content(user.name)
            expect(page).to_not have_content("Movimentação interna")
          end
        end
      end
    end

    # TODO
    # Remover esse teste
    context 'proxy document' do
      it "save a information on client's history" do
        user_sign_in
        visit sac_path(client.slug)

        click_link 'Autorização'
        click_link 'Procuração'

        visit sac_path(client.slug)
        within('.table:last-child') do
          expect(page).to_not have_content("Documento visualizado: Procuração")
          expect(page).to_not have_content(user.name)
          expect(page).to_not have_content("Movimentação interna")
        end
      end
    end

    context 'service document' do
      context 'creating a service document' do
        it "save a information on client's history" do
          user_sign_in
          visit sac_path(client.slug)

          click_link 'Autorização'
          click_link 'Nova Autorização de Serviço'

          within('#auth') do
            fill_in 'Valor do serviço', with: 'R$ 5.000,00'
            fill_in 'Forma de pagamento', with: '5x 1.000,00'
            fill_in 'Nome do outorgante', with: 'Alexandre Pato'
            fill_in 'Nacionalidade', with: 'Brasileira'
            fill_in 'Rg', with: '23.675.123-0'
            fill_in 'CPF', with: '109.857.725-65'
            fill_in 'Data Nasc.', with: '09/08/1985'
            fill_in 'Profissão', with: 'Jogador profissional'

            fill_in 'Cargo', with: 'Jogador'
            fill_in 'Agente', with: 'Nelinho'
            fill_in 'Órgão', with: 'SSP'
            fill_in 'Descrição do serviço', with: 'Bicho do jogo'
            click_button 'Salvar'
          end

          expect(page).to have_content("Autorização de serviço criada com sucesso.")

          visit sac_path(client.slug)
          within('.table:last-child') do
            expect(page).to have_content("Documento gerado: Autorização de serviço")
            expect(page).to have_content(user.name)
            expect(page).to have_content("Movimentação interna")
          end
        end
      end

      # TODO
      # Remover esse teste
      context 'viewing a service document' do
        it "save a information on client's history" do
          user_sign_in
          visit sac_path(client.slug)

          click_link 'Autorização'
          click_link '%05d' % client.service_documents.first.number

          visit sac_path(client.slug)
          within('.table:last-child') do
            expect(page).to_not have_content("Documento visualizado: Autorização de serviço")
            expect(page).to_not have_content(user.name)
            expect(page).to_not have_content("Movimentação interna")
          end
        end
      end
    end
  end
end
