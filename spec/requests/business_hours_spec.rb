# encoding: UTF-8
require 'spec_helper'

describe 'logged in as manager, consultant or operator' do
  let(:role) {%w(manager consultant operator)}
  let(:user){ create(:user, role: role.sample) }

  after {Timecop.return}

  context "accessing the system before open" do
    before {Timecop.freeze(Time.local(2014, 6, 17, 8, 44))}

    it "see the error message" do
      user_sign_in(user)

      visit clients_path
      page.should have_content "Você precisa estar logado para acessar essa página. Horário de funcionamento: Seg-Sex 08h45 | 18h30"
    end
  end

  context "accessing the system in business hours" do
    let!(:client){ create :client, user: user }
    before {Timecop.freeze(Time.local(2014, 6, 17, 12, 00))}

    it "should navigate normally" do
      user_sign_in(user)

      visit clients_path
      page.should have_content client.company_name
    end
  end

  context "accessing the system after close" do
    before {Timecop.freeze(Time.local(2014, 6, 17, 18, 31))}

    it "see the error message" do
      user_sign_in(user)

      visit clients_path
      page.should have_content "Você precisa estar logado para acessar essa página. Horário de funcionamento: Seg-Sex 08h45 | 18h30"
    end
  end
end

describe 'logged in as admin' do
  let(:admin){ create(:user, :admin) }
  let!(:client){ create :client }

  after {Timecop.return}

  context "accessing the system before open" do
    before {Timecop.freeze(Time.local(2014, 6, 17, 8, 44))}

    it "should navigate normally" do
      user_sign_in(admin)

      visit clients_path
      page.should have_content client.company_name
    end
  end

  context "accessing the system in business hours" do
    before {Timecop.freeze(Time.local(2014, 6, 17, 12, 00))}

    it "should navigate normally" do
      user_sign_in(admin)

      visit clients_path
      page.should have_content client.company_name
    end
  end

  context "accessing the system after close" do
    before {Timecop.freeze(Time.local(2014, 6, 17, 17, 16))}

    it "should navigate normally" do
      user_sign_in(admin)

      visit clients_path
      page.should have_content client.company_name
    end
  end
end
