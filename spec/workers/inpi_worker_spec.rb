require 'spec_helper'
require 'sidekiq/testing'

describe InpiWorker do
  let(:ziper){double}
  let(:mailer){double}
  let(:persist){double}
  let(:file_url){'some inpi file'}
  let(:zip_path){'/path/unziped_file'}

  it "enquee the job" do
    expect {
      InpiWorker.perform_async(file_url)
    }.to change(InpiWorker.jobs, :size).by(1)
  end

  context '#perform' do
    before do
      ConflictFile::Ziper
        .should_receive(:new)
        .with(file_url)
        .and_return(ziper)

      ziper.should_receive(:process)
      ziper
        .should_receive(:extracted_file)
        .twice
        .and_return(zip_path)

      ConflictFile::Persist
        .should_receive(:new)
        .with(zip_path)
        .and_return(persist)

      persist.should_receive(:find_and_save)

      InpiMailer
        .should_receive(:process_done)
        .with('unziped_file')
        .and_return(mailer)

        mailer.should_receive(:deliver)
    end

    it {InpiWorker.new.perform(file_url)}
  end
end
