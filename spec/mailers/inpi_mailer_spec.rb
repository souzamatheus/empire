# encoding: utf-8
require "spec_helper"

describe InpiMailer do
  describe ".process_done" do
    let(:filename) {"filename_1020.xml"}

    subject {InpiMailer.process_done(filename)}

    its(:from) {should == ["colidencia@dnabrasilmarcas.com.br"]}
    its(:to) {should == ["rafael@dnabrasilmarcas.com.br"]}
    its(:subject) {should == "[DNA Brasil] - Arquivo de colidência finalizado"}

    describe 'mail body' do
      it "should see the body message" do
        subject.body.encoded.should match(/O arquivo \"filename_1020.xml\" foi processado com sucesso./)
      end
    end
  end
end
