# encoding: utf-8
module IntegrationHelpers
  def user_sign_in(user=nil)
    user = user || User.last || create(:user)
    sign_in_with(user)
  end

  def disable_business_hours
    Settings.stub(:open?).and_return(true)
  end

  private

  def sign_in_with(user)
    visit root_path
    fill_in "user_login",    :with => user.login
    fill_in "user_password", :with => 'please'
    click_button "Entrar"
  end
end
