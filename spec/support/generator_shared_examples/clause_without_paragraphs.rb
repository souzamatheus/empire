shared_examples "a clause without paragraphs" do |clause_number, paragraph_numbers|
  let(:service_document){
    create(:service_document, :client_informations,
            client:   create(:client),
            contract: build(:contract))
  }

  let(:document){Prawn::Document.new}
  let(:clauses){described_class.new(document, service_document)}
  let(:inspector){PDF::Inspector::Text.analyze(clauses.document.render)}
  let(:parser){Prawn::Text::Formatted::Parser.to_array(text)}

  before {clauses.add_clauses_to_document}

  paragraph_numbers.each do |paragraph|
    context "clause #{clause_number.humanize}" do
      subject{inspector.strings}

      context "paragraph #{paragraph.humanize}" do
        let(:text){clauses.get_paragraph(clause_number, paragraph, node)}

        context "text" do
          let(:node){"title"}
          it {parser.should be_blank}
        end

        context "content" do
          let(:node){"content"}
          it {parser.should be_blank}
        end
      end
    end
  end
end
