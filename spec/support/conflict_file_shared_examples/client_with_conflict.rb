shared_examples "a client with conflict" do |brand, conflict, client_brand|
  let(:brands){[
    double(
      number:       "12345678",
      date:         "14/09/2012",
      class_name:   "590",
      company_name: "Fulano",
      inpi:         "0192837465",
      presentation: "Mista",
      nature:       "De Produto",
      brand:        brand,
      conflict:     client_brand,
      client:       client
    )
  ]}

  before do
    ConflictFile::Finder.any_instance.stub(:extract_brands).and_return(brands)
  end

  subject do
    described_class.new("some file").matcher
  end

  context "on conflicts" do
    let(:conflicts) {build(:conflict, value: conflict)}
    let!(:client) {create(:client, conflicts: [conflicts])}

    context "#{conflict}" do
      it {should have(1).item}
      it "retrieves the brand informations" do
        subject.first.number.should == "12345678"
        subject.first.date.should == "14/09/2012"
        subject.first.class_name.should == "590"
        subject.first.company_name.should == "Fulano"
        subject.first.inpi.should == "0192837465"
        subject.first.presentation.should == "Mista"
        subject.first.nature.should == "De Produto"
        subject.first.brand.should == brand
        subject.first.conflict.should_not be_blank
        subject.first.client.should == client
      end
    end
  end if conflict.present?

  context "on brands" do
    let(:client_brands) {build(:brand, name: client_brand)}
    let!(:client) {create(:client, brands: [client_brands])}

    context "#{client_brand}" do
      it {should have(1).item}
      it "retrieves the brand informations" do
        subject.first.number.should == "12345678"
        subject.first.date.should == "14/09/2012"
        subject.first.class_name.should == "590"
        subject.first.company_name.should == "Fulano"
        subject.first.inpi.should == "0192837465"
        subject.first.presentation.should == "Mista"
        subject.first.nature.should == "De Produto"
        subject.first.brand.should == brand
        subject.first.conflict.should_not be_blank
        subject.first.client.should == client
      end
    end
  end if client_brand.present?
end
