shared_examples "a client without conflict" do |brand, conflict|
  let(:brands){[double(brand: brand)]}

  before do
    ConflictFile::Finder.any_instance.stub(:extract_brands).and_return(brands)
  end

  subject do
    described_class.new("some file").matcher
  end

  let(:conflicts) {build(:conflict, value: conflict)}
  let!(:client) {create(:client, conflicts: [conflicts])}

  context "conflict #{conflict}" do
    it {should be_empty}
  end
end
