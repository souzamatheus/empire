require 'spec_helper'

describe NotificationPresenter do
  let(:user) { create(:user) }
  let!(:client) { create(:company, company_name: "Loldesign") }
  let(:presenter){ described_class.new(entries) }

  before {Timecop.freeze(Time.local(2013, 3, 25, 17, 15, 00))}
  after {Timecop.return}

  describe "#json" do
    let(:entries) {[Notification.last]}

    before do
      create(:notification, user: user, client: client, call_back_at: Time.now)
    end

    it { presenter.json.should == [{
	id:			entries.first.to_param,
	notification_type:	"Agendamento",
        company_name:           "Loldesign",
        cpf:                    nil,
        cnpj:                   "72516266000100",
        call_back_at:           "2013-03-25 17:15:00 -0300",
        formatted_call_back_at: "17:15",
        sac_type:               "scheduling intime",
        link:                   "/atendimento/cliente/#{client.id}",
        slug:                   "loldesign"
      }]}
  end

  describe "#sac_json" do
    let(:entries) do
      create(:notification,
	     user: user,
	     client: client,
	     call_back_at: call_back_at,
	     notification_type: notification_type)
    end

    context 'scheduling' do
      let(:notification_type) { Notification::Type::SCHEDULING }

      context "delayed" do
        let(:call_back_at) { 10.minutes.ago }
        it { presenter.sac_json[:sac_type].should eq("scheduling delayed") }
      end

      context "intime" do
        let(:call_back_at) { Time.now }
        it { presenter.sac_json[:sac_type].should eq("scheduling intime") }
      end

      context "ahead" do
        let(:call_back_at) { 10.minutes.from_now }
        it { presenter.sac_json[:sac_type].should eq("scheduling ahead") }
      end
    end

    context 'process_validity' do
      let(:notification_type) { Notification::Type::PROCESS_VALIDITY } 

      context "delayed" do
        let(:call_back_at) { 10.minutes.ago }
        it { presenter.sac_json[:sac_type].should eq("process_validity delayed") }
      end

      context "intime" do
        let(:call_back_at) { Time.now }
        it { presenter.sac_json[:sac_type].should eq("process_validity intime") }
      end

      context "ahead" do
        let(:call_back_at) { 10.minutes.from_now }
        it { presenter.sac_json[:sac_type].should eq("process_validity ahead") }
      end
    end
  end
end
