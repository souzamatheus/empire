require 'spec_helper'

describe ClientsProcessesController do
  login_user('admin')

  describe "GET /clientes/:client_id/processes/novo" do
    it "displays the page to create a new process" do
      client = mock_model(Client)
      client_process = mock_model(ClientProcess)
      Client.should_receive(:find).and_return(client)
      client.stub_chain(:client_processes, :build).and_return(client_process)

      get :new, client_id: client.to_param
      response.should render_template("new")
      assigns[:process].should == client_process
    end
  end

  describe "#create" do
    context 'when success' do
      let!(:client){ create(:client) }

      before do
        post :create,
          client_id: client.to_param,
          client_process: {number: '123'}
      end

      it{ response.should redirect_to(sacs_path(client, anchor: :process)) }
      it{ client.client_processes.size.should eq(1)}
      it{ flash[:notice].should =~ /Processo criado com sucesso/ }
    end

    context 'with error' do
      let(:client) {mock_model(Client)}

      before do
        Client.stub(:find).and_return(client)
        client.stub_chain(:client_processes, :create).and_return(false)

        post :create,
          client_id: client.to_param,
          client_process: {number: '123'}
      end

      it{ response.should redirect_to(sacs_path(client, anchor: :process)) }
      it{ flash[:error].should =~ /Houve um erro\. Verifique todos os campos/ }
    end
  end

  describe "#update" do
    context 'when success' do
      let(:notification_manager) { double('NotificationProcessManager') }
      let(:client_process){ client.client_processes.last }
      let!(:client) do
        create(:client, client_processes: [build(:client_process)])
      end

      before do
	NotificationProcessManager
	  .should_receive(:new)
	  .with(client)
	  .and_return(notification_manager)

	notification_manager.should_receive(:clean)

        put :update,
          client_id: client.to_param,
          id: client_process.to_param,
          client_process: {number: '102030'}
      end

      it{ response.should redirect_to(sacs_path(client, anchor: :process)) }
      it{ client_process.reload.number.should eq('102030')}
      it{ flash[:notice].should =~ /Processo atualizado com sucesso/ }
    end

    context 'with error' do
      let(:client) {mock_model(Client)}
      let(:client_process) {mock_model(ClientProcess)}

      before do
        Client.stub(:find).and_return(client)
        client.stub_chain(:client_processes, :find).and_return(client_process)
        client_process.should_receive(:update_attributes).and_return(false)

	NotificationProcessManager
	  .should_not_receive(:new)

        put :update,
          client_id: client.to_param,
          id: client_process.to_param,
          client_process: {number: '102030'}
      end

      it{ response.should redirect_to(sacs_path(client, anchor: :process)) }
      it{ flash[:error].should =~ /Houve um erro\. Verifique todos os campos/ }
    end
  end

  describe "#destroy" do
    let!(:client){ create(:client, client_processes: [attributes_for(:client_process)]) }

    let(:client_process){ client.client_processes.first }

    before{ delete(:destroy, client_id: client.to_param, id: client_process.id) }

      it{ response.should redirect_to(sacs_path(client, anchor: :process)) }
      it{ client.reload.client_processes.reject{|x| x.new_record? }.size.should eq(0)}
      it{ flash[:notice].should =~ /Processo removido com sucesso/ }
  end
end
