# encoding: utf-8
require 'spec_helper'

describe ConflictsController do
  login_user
  disable_business_hours

  describe "GET index" do
    before do
      Inpi::File
        .stub_chain(:all, :order_by)
        .with([:created_at, :desc])
        .and_return(double)
      get :index
    end

    it {response.should render_template(:index)}
    it {assigns[:files].should_not be_nil}
  end

  describe "GET show" do
    let(:file) {mock_model(Inpi::File)}

    before do
      Inpi::File
        .should_receive(:find)
        .with(file.to_param).and_return(file)

      file.should_receive(:entries).and_return([])
      get :show, file_id: file.to_param
    end

    it {response.should render_template(:show)}
    it {assigns[:conflicts].should_not be_nil}
  end

  describe "GET entry" do
    let(:file) {mock_model(Inpi::File)}

    before do
      Inpi::File
        .should_receive(:find)
        .with(file.to_param).and_return(file)

      file
        .stub_chain(:entries, :where)
        .with({number: "1234"})
        .and_return([double])

      get :entry, file_id: file.to_param, id: "1234"
    end

    it {response.should render_template(:entry)}
    it {assigns[:entry].should_not be_nil}
  end

  describe 'POST create' do
    before do
      InpiWorkerManager.stub(:new).and_return(manager)
    end

    context "with valid data" do
      let(:manager) {double(valid?: true)}

      before do
        InpiWorker.should_receive(:perform_async).with('some_file_url')
        post :create, inpi_file_url: 'some_file_url'
      end

      it {response.should redirect_to conflicts_path}
      it {flash[:notice].should == "Arquivo adicionado com sucesso. Em instantes ele aparecerá na fila de processamento."}
    end

    context "with invalid data" do
     let(:manager) {double(valid?: false)}

      before do
        post :create, inpi_file_url: 'some_file_url'
      end

      it {response.should redirect_to conflicts_path}
      it {flash[:notice].should == "Arquivo inválido ou já existe."}
    end
  end
end
