require 'spec_helper'

describe SacController do
  login_user
  disable_business_hours

  let(:client) { mock_model(Client, service_documents: [build(:service_document)]) }

  before do
    Client.should_receive(:find).with(client.to_param).and_return(client)
    client.stub_chain(:client_files, :build).and_return(double(ClientFile))
  end

  describe "GET /atendimento/cliente/1" do
    it "displays client" do
      get :show, id: client.to_param
      response.should render_template("show")
      assigns[:client].should == client
    end
  end

  describe "POST /atendimento/cliente" do
    let(:manager) { double SacManager }
    let(:sac) { mock_model Sac }

    def do_post
      post :create, id: client.to_param, sac_entry: {"these" => "params"}
    end

    context "with valid data" do
      before { manager.stub(:save).and_return(true) }

      it "successfully creates a new client" do
        SacManager.should_receive(:new).with(client, {"these" => "params"}).and_return(manager)

        do_post
        response.should redirect_to sac_path(client)
        flash[:notice].should == "Atendimento registrado com sucesso!"
      end
    end

    context "with invalid data" do
      before { manager.stub(:save).and_return(false) }

      it "fails to create a new client" do
        SacManager.should_receive(:new).with(client, {"these" => "params"}).and_return(manager)

        do_post
        response.should render_template("show")
        assigns[:client].should == client
        flash[:error].should == "Houve algum problema. Verifique os campos."
      end
    end
  end
end
