require 'spec_helper'

describe ContractsController do
  login_user('admin')

  describe "GET show" do
    let(:client){mock_model(Client)}
    let(:document){mock_model(ServiceDocument)}
    let(:contract){mock_model(Contract)}
    let(:generator){"pdf content"}
    let(:options){{
      type:         "application/pdf",
      filename:     "contrato.pdf",
      disposition:  "inline"
    }}

    before do
      Generator::Contract::Base.any_instance.stub(:generate).and_return(generator)
      Client.should_receive(:find).with(client.to_param).and_return(client)
      client.stub_chain(:service_documents, :find).with(document.to_param).and_return(document)
    end

    subject {get :show, client_id: client.to_param, document_id: document.to_param, id: contract.to_param}

    it {subject.status.should == 200}
    it {subject.body.should == "pdf content"}
    it {subject.header["Content-Disposition"].should == "inline; filename=\"contrato.pdf\""}
    it {subject.header["Content-Transfer-Encoding"].should == "binary"}
    it {subject.header["Content-Type"].should == "application/pdf"}
  end

  describe "GET new" do
    let(:client){create(:client)}
    let(:document){create :service_document, :client_informations, client: client}

    it "build a contract" do
      get :new, client_id: client.to_param, document_id: document.to_param
      assigns[:contract].should be_an_instance_of(Contract)
    end
  end

  describe "POST create" do
    let(:client){mock_model(Client)}
    let(:document){mock_model(ServiceDocument)}
    let(:contract){mock_model(Contract)}

    before do
      Client.should_receive(:find).with(client.to_param).and_return(client)
      client.stub_chain(:service_documents, :find).with(document.to_param).and_return(document)
    end

    def do_post
      post :create, client_id: client.to_param, document_id: document.to_param, contract: {"these" => "params"}
    end

    context "with valid data" do
      before {contract.stub(:save).and_return(true)}

      it "successfully creates a new contract" do
        TrackerDocument.should_receive(:register).with(client, an_instance_of(User), "Contract", "create")
        document.should_receive(:build_contract).with("these" => "params").and_return(contract)

        do_post
        response.should redirect_to(service_authorizations_path(client, anchor: :auth))
        flash[:notice].should == "Contrato gerado com sucesso."
      end
    end

    context "with invalid data" do
      before { contract.stub(:save).and_return(false) }

      it "fails to create a new contract" do
        TrackerDocument.should_not_receive(:register)
        document.should_receive(:build_contract).with("these" => "params").and_return(contract)

        do_post
        response.should render_template(:new)
        assigns[:contract].should == contract
      end
    end
  end
end
