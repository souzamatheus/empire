require 'spec_helper'

describe ClientsImagesController do
  login_user
  disable_business_hours

  describe "GET destroy" do
    let(:file) {mock_model(ClientImage)}
    let(:client) {mock_model(Client, client_images: file)}

    before do
      Client.stub(:find).with(client.to_param).and_return(client)
      file.should_receive(:find).with("1234").and_return(file)
      file.should_receive(:destroy).and_return(true)

      delete :destroy, client_id: client.to_param, id: "1234"
    end

    it {response.should redirect_to sac_path(client)}
    it {flash[:notice].should == 'Logo removido com sucesso.'}
  end
end
