# encoding: UTF-8
require 'spec_helper'

describe DocumentsController do
  login_user('admin')

  let(:client){ mock_model(Client) }

  describe "GET index" do
    let(:documents){ double }

    it "returns a list of documents" do
      Client.stub(:find).with(client.to_param).and_return(client)
      client.should_receive(:service_documents).and_return(documents)

      get :index, client_id: client.to_param
      response.should render_template(:index)
      assigns[:documents].should == documents
    end
  end

  describe "GET show" do
    let(:document){ mock_model(ServiceDocument) }

    it "return a specific document" do
      Client.stub(:find).with(client.to_param).and_return(client)
      client.should_receive(:service_documents).and_return(document)
      document.should_receive(:find).with(document.to_param).and_return(document)

      get :show, client_id: client.to_param, id: document.to_param
      response.should render_template(:service_authorization)
      assigns[:document].should == document
    end
  end

  describe "GET new" do
    it "render the new template" do
      ServiceDocument.should_receive(:new)
      Client.should_receive('find').and_return(client)

      get :new, client_id: client.to_param
      response.should render_template(:new)
    end
  end

  describe "POST create" do
    let(:manager) { double(document: double) }

    before do
      Client.stub(:find).with(client.to_param).and_return(client)
    end

    def do_post
      post :create, client_id: client.to_param, :service_document => {"these" => "params"}
    end

    context "with valid data" do
      before { manager.stub(:save).and_return(true) }

      it "successfully creates a new service document" do
        ServiceDocumentManager.should_receive(:new).with(client, "these" => "params").and_return(manager)
        TrackerDocument.should_receive(:register).with(client, an_instance_of(User), "ServiceDocument", "create")
        do_post

        response.should redirect_to(service_authorizations_path(client, anchor: :auth))
        flash[:notice].should == "Autorização de serviço criada com sucesso."
      end
    end

    context "with invalid data" do
      before { manager.stub(:save).and_return(false) }

      it "fails to create a new service document" do
        ServiceDocumentManager.should_receive(:new).with(client, "these" => "params").and_return(manager)
        TrackerDocument.should_not_receive(:register)
        do_post

        response.should render_template(:new)
        assigns[:document].should == manager.document
      end
    end
  end

  describe "GET destroy" do
    let(:client){ create(:client) }
    let(:manager){ ServiceDocumentManager.new(client, attributes_for(:service_document)) }
    before do
      manager.save
    end

    it "should delete a document" do
      expect do
        delete(:destroy, client_id: client.to_param, id: manager.document.to_param)
      end.to change { ServiceDocument.count }.by(-1)
    end
  end
end
