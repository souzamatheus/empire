#encoding: UTF-8
require 'spec_helper'

describe UsersController do
  login_user('admin')

  describe "GET /users" do
    it "displays the list of available users" do
      user = double
      users = double
      User.should_receive(:all).and_return(users)
      User.should_receive(:new).and_return(user)

      get :index
      response.should render_template("index")
      assigns[:users].should == users
      assigns[:user].should == user
    end
  end

  describe "POST /users" do
    context  let(:user) { mock_model(User) }

    def do_post
      post :create, :user => {"these" => "params"}
    end

    context "with valid data" do
      before { user.stub(:save).and_return(true) }

      it "successfully creates a new user" do
        User.should_receive(:new).with("these" => "params").and_return(user)

        do_post
        response.should redirect_to users_path
        flash[:notice].should == "Usuário criado com sucesso!"
      end
    end

    context "with invalid data" do
      before { user.stub(:save).and_return(false) }

      it "fails to create a new user" do
        User.should_receive(:new).with("these" => "params").and_return(user)

        do_post
        response.should render_template("index")
        assigns[:user].should == user
      end
    end
  end

  describe "GET /users/1/edit" do
    let(:user) { mock_model(User) }

    it "renders the form to update the user" do
      User.should_receive(:find).with(user.to_param).and_return(user)
      get :edit, :id => user.to_param
      response.should render_template("edit")
      assigns[:user].should == user
    end
  end

  describe "PUT /users/1" do
    let(:user) { mock_model(User) }

    before do
      User.stub(:find).with(user.to_param).and_return(user)
    end

    def do_put
      put :update, :id => user.to_param, :user => { "these" => "params" }
    end

    context "with valid data" do
      it "updates the user" do
        user.should_receive(:update_attributes).with("these" => "params").and_return(true)
        do_put
        response.should redirect_to users_path
        flash[:notice].should == "Usuário editado com sucesso!"
      end
    end

    context "with invalid data" do
      it "fails to update the users" do
        user.should_receive(:update_attributes).with("these" => "params").and_return(false)
        do_put
        response.should render_template("edit")
        assigns[:user].should == user
      end
    end
  end

  describe "PUT /configuracao/:login" do
    let(:user) { User.last }

    before do
      User.stub(:find).with(user.to_param).and_return(user)
    end

    def do_put
      put :configuration_update, :id => user.to_param, :user => { "these" => "params" }
    end

    context "with valid data" do
      it "updates the user" do
        user.should_receive(:update_attributes).with("these" => "params").and_return(true)

        do_put
        response.should redirect_to config_user_path(user)
        flash[:notice].should == "Usuário editado com sucesso!"
      end
    end

    context "with invalid data" do
      it "fails to update the users" do
        user.should_receive(:update_attributes).with("these" => "params").and_return(false)
        do_put
        response.should render_template("configuration")
        assigns[:user].should == user
      end
    end
  end

  describe "GET /user/1/reset_password" do
    let(:user) { mock_model(User) }

    before do
      User.stub(:find).with(user.to_param).and_return(user)
    end

    def do_get
      get :reset, :id => user.to_param
    end

    context "with valid data" do
      it "updates the user" do
        user.should_receive(:reset_password).and_return(true)
        do_get

        response.should redirect_to users_path
        flash[:notice].should == "Senha resetada com sucesso."
      end
    end

    context "with invalid data" do
      it "fails to reset the password" do
        user.should_receive(:reset_password).and_return(false)
        do_get

        response.should render_template("edit")
        assigns[:user].should == user
      end
    end
  end

  describe "GET block" do
    let(:user){ mock_model(User, name: 'Fulano') }

    before do
      User.stub(:find).with(user.to_param).and_return(user)
    end

    it "should lock the user" do
      user.should_receive(:lock_access!)

      get :lock, id: user.to_param
      response.should redirect_to users_path
      flash[:notice].should == "Usuário Fulano bloqueado com sucesso."
    end
  end

  describe "GET unblock" do
    let(:user){ mock_model(User, name: 'Fulano') }

    before do
      User.stub(:find).with(user.to_param).and_return(user)
    end

    it "should unlock the user" do
      user.should_receive(:unlock_access!)

      get :unlock, id: user.to_param
      response.should redirect_to users_path
      flash[:notice].should == "Usuário Fulano desbloqueado com sucesso."
    end
  end
end
