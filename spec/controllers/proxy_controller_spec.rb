require 'spec_helper'

describe ProxyController do
  login_user
  disable_business_hours

  describe "GET show" do
    let(:client){ mock_model(Client) }
    let(:document){ mock_model(ServiceDocument) }

    before do
      Client.stub(:find).with(client.to_param).and_return(client)
      client.stub_chain(:service_documents, :find).with("1234").and_return(document)

      get :show, client_id: client.to_param, id: "1234"
    end

    it { response.should render_template(:show) }
    it { assigns[:document].should == document }
  end
end
