# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :inpi_file, class: Inpi::File do
    name "some_file.xml"
  end
end
