# encoding: UTF-8
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :client_process do
    number { (1..1000).to_a.sample }
  end
end
