# encoding: UTF-8
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :client do
    company_name        { Faker::Company.name }
    cpf                 '873.461.269-63'
    document_type       :people
    annuity             { %w(may november).shuffle.first }
    email               { Faker::Internet.email }
    site                { Faker::Internet.domain_name }
    address             { Faker::Address.street_address }
    number              134
    neighborhood        'Mooca'
    city                'São Paulo'
    state               'SP'
    postal_code         '03020072'
    primary_phone       '11 9192-9394'
    contact             'Fulano da Silva'
    user
  end

  factory :company, parent: :client do
    cpf           nil
    cnpj          '72.516.266/0001-00'
    document_type :company
  end
end
