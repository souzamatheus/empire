# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :historic do
    description   Faker::Lorem.paragraph
    status        SacStatus::CALL_BACK
    call_back_at  Time.now
  end
end
