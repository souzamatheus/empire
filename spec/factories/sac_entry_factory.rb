# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sac_entry do
    description   Faker::Lorem.paragraph
    status        SacStatus::CALL_BACK
    call_back_at  Time.now
    user
    client
  end
end
