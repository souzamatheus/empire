require 'spec_helper'

describe ClientsFilesHelper do
  describe '#file_name' do
    context 'with filename' do
      let(:client_file) {double('ClientFile', filename: 'my_file.png')}
      it {helper.file_name(client_file).should == 'my_file.png'}
    end

    context 'without filename' do
      let(:client_file) {double('ClientFile', filename: nil, file: double(path: 'by/path/file.txt'))}
      it {helper.file_name(client_file).should == 'file.txt'}
    end
  end
end
