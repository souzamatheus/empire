env :PATH, ENV['PATH']
set :output, '/var/log/client_process_outdated.log'

every :day, :at => '06:00am' do
  rake 'client_process:outdated'
end
