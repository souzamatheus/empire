require 'sidekiq/web'

DnaBrasil::Application.routes.draw do
  devise_for :users, :path_names => { :sign_in => 'login', :sign_out => 'logout' }

  authenticate :user, lambda { |u| u.role == 'admin' } do
    mount Sidekiq::Web => '/sidekiq'
  end

  # CLIENT PROCESSES
  get '/clientes/:client_id/processes'            => 'clients_processes#index', as: :client_processes
  get '/clientes/:client_id/processes/novo'       => 'clients_processes#new', as: :new_client_process
  post '/clientes/:client_id/processes'           => 'clients_processes#create'  , as: :client_process
  delete '/clientes/:client_id/processes/:id'     => 'clients_processes#destroy', as: :destroy_client_processes
  get '/clientes/:client_id/processes/:id/editar' => 'clients_processes#edit', as: :edit_client_processes
  put '/clientes/:client_id/processes/:id'        => 'clients_processes#update', as: :update_client_process

  # CLIENT
  get '/clientes' => 'clients#index', as: :clients
  get '/cliente/novo' => 'clients#new', as: :new_client

  delete '/cliente/:id/remove-arquivo/:file_id' => 'clients#destroy_file', as: :destroy_file_client

  get '/cliente/:id/editar' => 'clients#edit', as: :edit_client

  post '/clientes' => 'clients#create'
  get '/clients/filtros' => 'clients#filter', as: :filter_clients
  put '/cliente/:id' => 'clients#update', as: :client
  delete '/cliente/:id' => 'clients#destroy'

  # REPORTS
  get '/relatorios' => 'reports#index', as: :reports
  get '/relatorios/filtros' => 'reports#filter', as: :filter_reports

  # USERS
  get   '/usuarios'                   => 'users#index',   as: :users
  get   '/usuario/:id'                => 'users#edit',    as: :edit_user
  get   '/usuario/reset_password/:id' => 'users#reset',   as: :reset_user
  post  '/usuarios'                   => 'users#create',  as: :user
  put   '/usuarios/:id'               => 'users#update',  as: :user
  get   '/usuario/:id/lock'           => 'users#lock',    as: :lock_user
  get   '/usuario/:id/unlock'         => 'users#unlock',  as: :unlock_user

  # USER CONFIG
  get '/configuracao/:id' => 'users#configuration', as: :config_user
  put '/configuracao/:id' => 'users#configuration_update'

  # SAC
  get  '/atendimento/cliente/:id' => 'sac#show'  , as: :sac
  post '/atendimento/cliente/:id' => 'sac#create', as: :sacs

  # GROW
  get '/grow' => 'grow#index'
  get '/grow/:notification_id' => 'grow#show'

  # DOCUMENTS
  get '/documentos/:client_id/servicos' => 'documents#index', as: :service_authorizations
  get '/documentos/:client_id/servico/novo' => 'documents#new', as: :new_service_authorization
  get '/documentos/:client_id/servico/:id' => 'documents#show', as: :service_authorization
  post '/documentos/:client_id/servico' => 'documents#create', as: :service_documents
  delete '/documentos/:client_id/remover/:id' => 'documents#destroy', as: :remove_service_authorization

  # CEP
  get '/servicos/cep/:cep' => 'custom_services#cep'

# PROXY DOCUMENT
  get '/documents/:client_id/servico/:id/procuracao' => 'proxy#show', as: :proxy_document

  # CONTRACT
  get '/documentos/:client_id/servico/:document_id/contrato'      => 'contracts#show',    as: :contract
  get '/documents/:client_id/servico/:document_id/contrato/novo'  => 'contracts#new',     as: :new_contract
  post '/documentos/:client_id/servico/:document_id/contrato'     => 'contracts#create',  as: :contracts

  # INPI
  get '/colidencias' => 'conflicts#index', as: :conflicts
  get '/colidencias/:file_id/registros' => 'conflicts#show', as: :conflict
  get '/colidencias/:file_id/registros/:id' => 'conflicts#entry', as: :entry
  post '/colidencias/' => 'conflicts#create', as: :send_conflict_file

  # AWARE DOCUMENT
  get '/documentos/:client_id/ciencia' => 'awares#show', as: :aware

  # CLIENT IMAGES
  delete '/client_images/:client_id/remover/:id' => 'clients_images#destroy', as: :remove_client_image

  root :to => 'clients#index'
end
