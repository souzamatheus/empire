require "bundler/capistrano"
require "sidekiq/capistrano"
require "capistrano-rbenv"

#production = ENV['DEPLOY_ENV'] == 'production'

set :application, "empire"
set :user       , "deployer"
#set :server_ip  , production ? '104.131.101.3' : '181.215.97.82'
set :server_ip  , '104.236.142.167'

set :scm        , :git
set :repository , "git@bitbucket.org:souzamatheus/empire.git"
set :branch     , "master"

set :deploy_via, :remote_cache
set :deploy_to, "/home/deployer/apps/#{application}"

set :use_sudo, false

set :rbenv_ruby_version, '1.9.3-p547'

# SIDEKIQ CONFIG
set :sidekiq_cmd, "bundle exec sidekiq -d -L log/sidekiq.log -e production"
set :sidekiqctl_cmd, "bundle exec sidekiqctl"
set :sidekiq_timeout, 10
set :sidekiq_role, :app
set :sidekiq_pid, "#{current_path}/tmp/pids/sidekiq.pid"
set :sidekiq_processes, 1

# WHENEVER CONFIG
set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

server server_ip, :web, :app, :db, primary: true

before "deploy:assets:precompile", "bundle:install"

namespace :deploy do
  task :setup_config, roles: :app do
    sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/#{application}"
    run "mkdir -p #{shared_path}/uploads"
    run "mkdir -p #{shared_path}/config"
    put File.read("config/mongoid.sample.yml"), "#{shared_path}/config/mongoid.yml"
    puts "Now edit the config files in #{shared_path}."
  end
  after "deploy:setup", "deploy:setup_config"

  task :start do ; end
  task :stop  do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
   # if production
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
    #else
    #  run "kill `cat #{shared_path}/tmp/pids/server.pid` || true"
    # run "#{current_path}/script/rails s -d"
    #end
  end

  task :create_uploads_symlink, roles: :web do
    puts "CREATING simlink to uploads folder"
    run "ln -nfs #{shared_path}/uploads #{current_path}/public/uploads"
    run "ln -nfs #{shared_path}/config/mongoid.yml #{current_path}/config/mongoid.yml"
  end
  after "deploy", "deploy:create_uploads_symlink"
end
