# encoding: utf-8
module Generator
  module AwareDocument
    class Base
      require "prawn"

      def initialize(client)
        @document = Prawn::Document.new(margin: [80, 40, 80, 40])
        @client = client
        @document.font("Times-Roman")
        @document.default_leading = 5
      end

      def generate
        header
        document_title
        document_content
        add_city_and_date
        add_client_signature
        footer
        add_page_numbers
        @document.render
      end

      private

      def add_page_numbers
        options = {
          at: [@document.bounds.right - 140, -20],
          width: 150,
          align: :right,
          start_count_at: 1,
        }

        @document.number_pages("<page>/<total>", options)
      end

      def add_client_signature
        text = @client.company_name
        @document.text_box("_" * (text.length + 10), at: [0, 205], align: :right)
        @document.text_box("#{text}", at: [0, 180], align: :right)
      end

      def add_city_and_date
        @document.text_box("#{@client.city}, #{I18n.l(Time.now, format: :local)}", at: [0, 280], align: :right)
      end

      def footer
        @document.repeat(:all) do
          @document.bounding_box([0, @document.bounds.bottom - 35], width: 540, height: 100) do
            @document.stroke_horizontal_rule
            @document.move_down(5)
            @document.text("Rua Bom Pastor, nº 2100, 7º Andar, Sala 707, Ipiranga, CEP 04203-002, São Paulo - SP", align: :center)
          #  @document.text("Fone: (11) 5061.1161 www.dnabrasilmarcas.com.br", align: :center)
            @document.text("www.empiremarcas.com.br", align: :center)
          end
        end
      end

      def document_content
        @document.text(I18n.t("generator.aware.term", company_name: I18n.t('project.company_name')), align: :justify, inline_format: true)
      end


      def document_title
        @document.move_down(40)
        @document.text("<u> CARTA DE CIÊNCIA </u>", size: 14, align: :center, inline_format: true)
        @document.move_down(80)
      end

       def header
        @document.repeat(:all) do
          @document.move_up(60)
          @document.image(File.join(Rails.root, "app/assets/images", "logo.png"), width: 80, height: 40)
          @document.move_up (8)
          @document.text("EMPIRE - GESTÃO DE MARCAS", align: :right, size: 12)
          @document.stroke_horizontal_rule
          @document.move_down(48  )
        end
      end
    end
  end
end
