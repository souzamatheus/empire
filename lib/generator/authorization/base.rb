# encoding: utf-8
module Generator
  module Authorization
    class Base 
      require "prawn"
      require 'brazilian-rails'
       include ApplicationHelper
        

      def initialize(service_document)
        @document = Prawn::Document.new(margin: [30, 40, 30, 40])
        @service_document = service_document
        @document.font("Helvetica")
        @document.default_leading = 5
      end

      def generate
        document_content
        @document.render
      end

      private

      def number_to_currency(*args)
        ActionController::Base.helpers.number_to_currency(*args)
      end

      def document_content
        @document.bounding_box([120, @document.cursor], :width => 410) do
          
          @document.rounded_rectangle [0,@document.cursor], 410, 30, 6
          @document.move_down 8
          @document.text(" <b> AUTORIZAÇÃO DE SERVIÇO ", size: 20, align: :center, inline_format: true)
        end

        @document.move_up 36
        
        @document.bounding_box([0,@document.cursor], :width => 90) do 
          @document.rounded_rectangle [0,@document.cursor], 110, 60, 7
          @document.move_down 8
          @document.image(File.join(Rails.root, "app/assets/images", "logo.png"), :at => [10,0],  width: 90, height: 45)       
        end

        @document.move_down 30
          
        @document.bounding_box([120, @document.cursor], :width => 310) do
          @document.rounded_rectangle [0,@document.cursor], 320, 22, 4
          @document.move_down 6
          @document.text("<b> ASSESSORIA DA PROPRIEDADE INDUSTRIAL", size: 14, align: :center, inline_format: true)
          
        end
        
        @document.move_up 28
       
        @document.bounding_box([450, @document.cursor], :width => 80) do
          @document.rounded_rectangle [0,@document.cursor], 80, 22, 4  
          @document.move_down 5
          @document.text("  <color rgb='ff0000'>     N° 0#{@service_document.number}", size: 16, align: :center, inline_format: true, :color => '0000FF') 
          @document.move_down 5
        end
        
        text = @service_document.company_name
       
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text(" CLIENTE: <b>#{(text).upcase}</b>", size: 10, inline_format: :true)
          end
          @document.stroke_bounds
        end

        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
             @document.text("ENDEREÇO: <b>#{(@service_document.full_address).upcase}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end

         @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do 
            @document.move_down 5
            @document.text(" BAIRRO: <b>#{(@service_document.neighborhood).upcase}</b>                    CIDADE: <b>#{(@service_document.city).upcase}</b>                 <color rgb= '696969'>   ESTADO: <b>#{(@service_document.state).upcase} ", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
         @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text(" CPF/CNPJ: <b>#{formatted_cpf(@service_document.client_document) || formatted_cnpj(@service_document.client_document)} <b>", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
         @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do 
            @document.move_down 5
            @document.text("TELEFONE: <b>#{@service_document.phone}</b>                         CONTATO: <b>#{(@service_document.contact).upcase}</b>                      CEP: <b>#{@service_document.postal_code}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
          @document.move_down 5
        end
        
         @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("AUTORIZAMOS A #{I18n.t('project.company_name').upcase}, A EXECUTAR NO(A) #{(@service_document.agency).upcase}  OS SERVIÇOS DISCRIMINADOS: ", size: 10, inline_format: true)
            @document.text("<b>#{(@service_document.description).upcase}",size:10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.move_down 5
         num = @service_document.amount
         num = BigDecimal(num.gsub('.', '').gsub(',', '.'))

         @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("O PRESENTE SERVIÇO IMPORTA EM R$: <b>#{number_to_currency(num)} (#{(num.por_extenso_em_reais).upcase})",position: 5, size: 10, inline_format: true)
          end
          @document.stroke_bounds 
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("FORMA DE PAGAMENTO: <b>#{(@service_document.payment).upcase}", size: 10, inline_format: :true)
          end
          @document.stroke_bounds
          @document.move_down 5
        end
        
         @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 7
            @document.text("<b> DADOS PARA PROCURAÇÃO", size: 12,align: :center, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("NOME DO OUTORGANTE: <b>#{(@service_document.person_name).upcase}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("NACIONALIDADE: <b>#{(@service_document.nationality).upcase}</b>                    ESTADO CIVIL: <b>#{(@service_document.relationship_status_humanize).upcase}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        date = @service_document.birth_date
       @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text(" RG: <b>#{@service_document.rg}</b>                    DATA NASCIMENTO: <b>#{date.to_date.to_s_br}</b>                      CPF: <b>#{formatted_cpf(@service_document.cpf)}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do 
            @document.move_down 5
            @document.text(" PROFISSÃO: <b>#{(@service_document.occupation).upcase}</b> ", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("CARGO: <b>#{(@service_document.position).upcase}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text(" OBSERVAÇÃO: <b>#{(@service_document.observations).upcase}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.move_down 5
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text(" AGENTE: <b>#{(@service_document.agent).upcase}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("  CLIENTE: <b>#{(@service_document.company_name).upcase}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do 
            @document.move_down 5
            @document.text(" LOCAL/DATA: <b>#{(@service_document.city).upcase}, #{I18n.l(@service_document.created_at, format: :local).upcase}", size: 10, inline_format: true)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("ASSINATURA:", inline_format: true, size: 10)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("CARIMBO:", inline_format: true, size: 10)
          end
          @document.stroke_bounds
        end
        
        @document.bounding_box([0, @document.cursor], :width => 530) do 
          @document.bounding_box([3,@document.cursor], :width => 527) do
            @document.move_down 5
            @document.text("<b>LEGALIZAÇÃO EM GERAL: </b>E por estarem justos e contratados, firmam o presente em 02 (duas) vias, selando desta forma os " +
                         "compromissos acima especificados, nas condições tratadas e aceitas por ambas as partes, não cabendo dúvida ou reclamação posterior, " +
                         "tendo em vista os termos. Quaisquer despachos relativos ao pedido em referência, serão prontamente levados ao conhecimento da "+
                         "Contratante, correndo por conta e risco da mesma, quaisquer inobservâncias ao cumprimento dos prazos estipulados na legislação em " +
                         "vigor Lei 9.279 de 14 de maio de 1996. Art. 49 - Lei 8.078 - O contratante não poderá cancelar o mesmo após sete dias da assinatura.", size: 9, inline_format: true)
          end
          @document.stroke_bounds
        end
      end
    end
  end
end
