module ConflictFile
  class Persist

    def initialize(file)
      @file = file
      @inpi_file = Inpi::File.find_or_create_by(name: File.basename(@file))
    end

    def find_and_save
      @informations = Finder.new(@file).matcher
      save_to_database
    end

    private

    def save_to_database
      @informations.each do |info|
        @inpi_file.entries.create(
          number:        info.number,
          date:          info.date,
          class_name:    info.class_name,
          company_name:  info.company_name,
          inpi:          info.inpi,
          presentation:  info.presentation,
          nature:        info.nature,
          brand:         info.brand,
          conflict:      info.conflict,
          client:        info.client
        ) if @inpi_file.entries.where(number: info.number, file: @inpi_file).empty?
      end

      @inpi_file.finish
    end
  end
end
