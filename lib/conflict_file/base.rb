# encoding: UTF-8
require 'nokogiri'

module ConflictFile
  class Base

    XML_ATTRIBUTES = {
      number:       '@numero',
      date:         '@data-deposito',
      company_name: 'titulares titular @nome-razao-social',
      inpi:         'despachos despacho @codigo',
      presentation: 'marca @apresentacao',
      nature:       'marca @natureza',
      brand:        'marca nome'
    }

    def initialize(file)
      @file = file
    end

    def extract_brands
      brands.inject([]) do |infos, node|
        parent = node.parent

        infos.push(OpenStruct.new(
          number:       node_text(parent, :number),
          date:         node_text(parent, :date),
          class_name:   formatted_class_name(parent),
          company_name: node_text(parent, :company_name),
          inpi:         node_text(parent, :inpi),
          presentation: node_text(parent, :presentation),
          nature:       node_text(parent, :nature),
          brand:        node_text(parent, :brand)
        ))
      end
    end

    private

    def brands
      doc = Nokogiri::XML(File.open(@file))
      doc.xpath('//revista/processo/marca')
    end

    def formatted_class_name(node)
      edition = node.css('classe-nice @edicao')
      code = node.css('classe-nice @codigo')

      "NCL(#{edition}) #{code}"
    end

    def node_text(node, attribute)
      node.css(XML_ATTRIBUTES[attribute]).text
    end
  end
end
