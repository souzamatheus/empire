module ConflictFile
  class Ziper

    def initialize(file_url)
      @url        = URI.parse(file_url)
      @filename   = File.basename(@url.path)
      @file_path  = "/tmp/#{@filename}"
    end

    def process
      download
      unzip
    end

    def extracted_file
      "/tmp/#{@filename.match(/\d+/)}.xml"
    end

    private

    def unzip
      system("unzip -o #{@file_path} -d /tmp")
    end

    def download
      Net::HTTP.start(@url.host) do |http|
        http.open_timeout = 60 * 10 # 10 minutes
        http.read_timeout = 60 * 10 # 10 minutes

        resp = http.get(@url.path)
        open(@file_path, "wb") do |file|
          file.write(resp.body)
        end
      end
    end
  end
end
