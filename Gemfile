source 'https://rubygems.org'
ruby '1.9.3'

gem "bootstrap-sass", "= 2.0.4.0"
gem "cancan",         ">= 1.6.8"
gem "carrierwave",    "~> 0.6.0"
gem "ckeditor"
gem 'cpf_cnpj'
gem "devise",         "~> 2.2.4"
gem 'enumerate_it'
gem "fog",            "~> 1.20.0"
gem "humanize",       "~> 1.1.0"
gem "hominid",        ">= 3.0.5"
gem "jquery-rails"
gem "mini_magick"
gem "media_magick",   git: "git://github.com/nudesign/media_magick.git"
gem "mongoid",        "~> 3.1.7"
gem "mongoid_rails_migrations", "~> 1.0.0"
gem "mongoid-sequence", git: "git://github.com/everaldo/mongoid-sequence.git"
gem 'mongoid_slug',   '~> 3.2.1'
gem 'prawn'
gem "rails",          "~> 3.2.17"
gem "rolify",         ">= 3.2.0"
gem "simple_form"
gem 'therubyracer'
gem 'thin',           '~> 1.5.1'
gem 'unicode_utils'
gem "will_paginate_mongoid"
gem 'state_machine',  "~> 1.1.2"
gem 'rails-i18n'
gem 'nokogiri',       '~> 1.5.0'
gem 'sidekiq',        '~> 2.17.3'
gem 'sinatra',        '>= 1.3.0', :require => nil
gem 'slim',           '~> 2.0.1'
gem 'whenever',       '~> 0.9.4', :require => false

gem 'brazilian-rails'

group :assets do
  gem 'sass-rails',         '~> 3.2.3'
  gem 'coffee-rails',       '~> 3.2.1'
  gem 'uglifier',           '>= 1.0.3'
  gem 'compass-rails',      '1.0.3'
  gem 'compass-960-plugin', '0.10.4'
end

group :production do
  #gem "unicorn", ">= 4.3.1"
  gem  'puma'
end

group :development do
  gem 'awesome_print'
  gem 'better_errors'
  gem "eventmachine",     '~> 1.0.0'
  gem 'foreman'
  gem 'capistrano',       '~> 2.15.4'

  gem 'capistrano-rbenv', '~> 1'
 

  gem 'rb-fsevent',       '= 0.9.1'
  gem 'growl'
  gem 'binding_of_caller'
  gem 'mailcatcher', '~> 0.5.12'
end

group :development, :test do
  gem "factory_girl_rails", ">= 4.0.0"
  gem 'ffaker'
  gem 'pry'
  gem 'rspec-rails'       , '~> 3.1.0'
end

group :test do
  gem "database_cleaner",   ">= 0.8.0"
  gem 'email_spec',         '~> 1.6.0'
  gem 'launchy'
  gem "mongoid-rspec",      ">= 1.4.6"
  gem "pdf-inspector",      "~> 1.1.0", :require => "pdf/inspector"
  gem 'poltergeist',        '~> 1.5.1'
  gem 'simplecov',          :require => false
  gem 'timecop',            '~> 0.6.3'
  gem 'rspec-mocks', 	    '~> 3.1.3'
  gem 'rspec-its',          '~> 1.1.0'
  gem 'rspec-activemodel-mocks', '~> 1.0.1'
  gem 'rspec-expectations', '~> 3.1.2'
  gem 'rspec-collection_matchers', '~> 1.1.2'
end
